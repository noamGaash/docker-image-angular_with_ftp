# docker image: angular_with_ftp

this repository has both angular cli and git ftp tools.
perfect for CI/CD processes

## usage example - bitbucket pipeline
### bitbucket-pipelines.yml
```
image: noamgaash/docker_image-angular_with_ftp

pipelines:
  default:
    - step:
        caches:
          - node
        script:
          - npm install
          - ng build --configuration=production
          - sh deploy.sh
```
### deploy.sh
```
cd dist/angular_app_name
git config --global user.email "noam.gaash@gmail.com"
git config --global user.name "noam gaash (auto deployment script)"
git init
git add .
git commit -m "Initial commit of dist/dancipedia"
if [ -n "$FTP_USERNAME" ]
then
	git config git-ftp.password $FTP_PASSWORD
	git config git-ftp.url $ftp_URL
	git config git-ftp.user $FTP_USERNAME
fi
git ftp push -f --all
```
do not forget - define `$FTP_USERNAME`, `$FTP_PASSWORD` and `$ftp_URL`

:bulb: :smiley:
