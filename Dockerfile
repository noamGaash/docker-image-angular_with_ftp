FROM node:8.9 as angular_with_ftp

LABEL authors="Noam Gaash"


# Angular CLI
RUN npm install -g @angular/cli

# git ftp
RUN apt-get update
RUN apt-get -qq install git-ftp